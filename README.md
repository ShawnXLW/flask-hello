# Flask Hello World

A simple Python web app that echoes `Hello World`.

## Instructions

### Build

To build a docker image from source:

```
docker build -t flask-hello:latest .
```

### Run

To run the container in foreground (prints logs on screen):

```
docker run -p 5000:5000 flask-hello:latest
```

To run the container in background:

```
docker run --name flask-hello -d -p 5000:5000 flask-hello:latest
```

And you can view logs by
```
docker logs flask-hello
```

## Design Decisions

### Image

Base image `python:alpine` was chosen.

Reasoning:

##### Image size

When using `ubuntu:16.05` after building the image size is `427MB`! Compared to only `90MB` when using `python:alpine`

##### Efficiency

Only pack what you need. For this simple web app, we just need python. This ensures the app runs more efficiently.

### Cache packages
Fetch all dependancies, in this case only Flask, during docker build to ready python app for execution.
